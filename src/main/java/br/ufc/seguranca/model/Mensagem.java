package br.ufc.seguranca.model;

import static br.ufc.seguranca.util.Constants.PATH_KEYS;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.ufc.seguranca.util.AsymmetricCryptography;

@Entity
public class Mensagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String conteudo;
	private boolean status;
	
	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE })
	private Usuario remetente;
	
	@ManyToOne
	private Conversa conversa;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Usuario getRemetente() {
		return remetente;
	}

	public void setRemetente(Usuario remetente) {
		this.remetente = remetente;
	}
	
	public Conversa getConversa() {
		return conversa;
	}
	
	public void setConversa(Conversa conversa) {
		this.conversa = conversa;
	}
	
	public String decrypt(Usuario usuario) {
		try {
			AsymmetricCryptography asymmetricCryptography = new AsymmetricCryptography();
			PublicKey publicKey = asymmetricCryptography.getPublic(PATH_KEYS+usuario.getUsername()+"/public_key");
			String conteudo_decrypt = asymmetricCryptography.decryptText(this.conteudo, publicKey);
			
			return conteudo_decrypt;
			
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeySpecException | IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
