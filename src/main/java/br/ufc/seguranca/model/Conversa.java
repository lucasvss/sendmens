package br.ufc.seguranca.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Conversa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	private Usuario participanteIniciante;
	
	@ManyToOne
	private Usuario participanteChamado;
	
	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
	@JoinColumn(name = "mensagem_id", unique = false)
	private List<Mensagem> mensagens;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getParticipanteIniciante() {
		return participanteIniciante;
	}

	public void setParticipanteIniciante(Usuario participanteIniciante) {
		this.participanteIniciante = participanteIniciante;
	}

	public Usuario getParticipanteChamado() {
		return participanteChamado;
	}

	public void setParticipanteChamado(Usuario participanteChamado) {
		this.participanteChamado = participanteChamado;
	}

	public List<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}
	
	public Mensagem getLastMensagem() {
		if(!this.mensagens.isEmpty()) {
			return this.mensagens.get(this.mensagens.size() - 1);
		}
		
		return null;
	}
}
