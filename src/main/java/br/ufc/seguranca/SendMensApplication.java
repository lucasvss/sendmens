package br.ufc.seguranca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendMensApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendMensApplication.class, args);
	}
}
