package br.ufc.seguranca.util;

public class Constants {

	//Páginas
	public static final String PAGINA_INICIAL = "principal/index";
	public static final String PAGINA_CADASTRO = "usuario/cadastro-usuario";
	public static final String PAGINA_EDITAR = "usuario/editar-usuario";
	
	//Redirects
	public static final String REDIRECT_LOGIN = "redirect:/login";
	public static final String REDIRECT_LOGOUT = "redirect:/logout";
	public static final String REDIRECT_PRINCIPAL = "redirect:/";
	public static final String REDIRECT_EDITAR = "redirect:/";
	public static final String REDIRECT_APAGAR_MENSAGEM = "redirect:/";

	//Mensagens
	public static final String MESSAGE_STATUS_SUCCESS = "success";
	public static final String MESSAGE_TITULO_CADASTRO_USUARIO = "Cadastro realizado com sucesso!";
	public static final String MESSAGE_TITULO_EDITAR_USUARIO = "Dados editados com sucesso";
	public static final String MESSAGE_TITULO_RESETAR_SENHA = "Email enviado!";
	public static final String MESSAGE_TITULO_INSERIR_CONTATO = "Contato Inserido!";
	
	
	public static final String MESSAGE_CONTEUDO_CADASTRO_USUARIO = "Sua conta foi cadastrada com sucesso!";
	public static final String MESSAGE_CONTEUDO_EDITAR_USUARIO = "Seus dados foram editados com sucesso!";
	public static final String MESSAGE_CONTEUDO_RESETAR_SENHA = "Um email foi enviado contendo as informações para resetar sua senha";
	public static final String MESSAGE_CONTEUDO_INSERIR_CONTATO = "O Contato foi inserido com sucesso!";
	
	public static final String MESSAGE_STATUS_ERROR = "error";
	public static final String MESSAGE_TITULO_INSERIR_CONTATO_ERROR = "Erro ao inserir o Contato";
	
	public static final String MESSAGE_CONTEUDO_INSERIR_CONTATO_ERROR = "Houve um problema ao tentar inserir o contato";

	
	//Errors
	public static final String ERROR_ADICIONAR_AMIGO = "Seu amigo não é um usuário";
	
	//Path Keys
	public static final String PATH_KEYS = "/home/lucasvss/eclipse-workspace/sendmens/src/main/resources/";
	
}
	