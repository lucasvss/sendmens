package br.ufc.seguranca.service;

import java.util.List;

import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Usuario;

public interface ConversaService {

	void iniciarConversa(Usuario  usuario, Integer idAmigo);
	void excluirConversa(Integer idConversa);
	Conversa conversa(Integer idConversa);
	Conversa conversa(Usuario usuario, Integer idAmigo);
	List<Conversa> conversas(Usuario usuario);
}
