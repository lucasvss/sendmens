package br.ufc.seguranca.service;

import br.ufc.seguranca.model.Usuario;

public interface UsuarioService {

	void cadastrarUsuario(Usuario usuario);
	void alterarUsuario(Usuario usuario);
	void excluirUsuario(Integer id);
	Usuario recuperarUsuario(Integer id);
}
