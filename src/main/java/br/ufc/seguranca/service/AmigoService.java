package br.ufc.seguranca.service;

import br.ufc.seguranca.exception.SendMensException;
import br.ufc.seguranca.model.Amigo;
import br.ufc.seguranca.model.Usuario;

public interface AmigoService {

	void adicionarAmigo(Amigo amigo, Usuario usuario) throws SendMensException;
	void adicionarAmigo(Integer idAmigo, Usuario usuario);
	void alterarAmigo(Amigo amigo);
	void bloquearAmigo(Integer idAmigo);
	void removerAmigo(Integer idAmigo);
	Amigo buscarAmigo(Integer idAmigo);
	Amigo buscarAmigoConversa(Integer idConversa, Usuario usuario);
}
