package br.ufc.seguranca.service.impl;

import static br.ufc.seguranca.util.Constants.PATH_KEYS;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Mensagem;
import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.repository.ConversaRepository;
import br.ufc.seguranca.repository.MensagemRepository;
import br.ufc.seguranca.repository.UsuarioRepository;
import br.ufc.seguranca.service.MensagemService;
import br.ufc.seguranca.util.AsymmetricCryptography;

@Service
public class MensagemServiceImpl implements MensagemService {
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private ConversaRepository conversaRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private AsymmetricCryptography asymmetricCryptography;
	
	@Override
	public void enviarMensagem(String conteudo, Usuario usuario, Integer idConversa) {
		Usuario usuarioRecuperado = usuarioRepository.findOne(usuario.getId());
		if(usuarioRecuperado != null) {
			Conversa conversa = conversaRepository.findOne(idConversa); 
			
			try {
				asymmetricCryptography = new AsymmetricCryptography();
				
				PrivateKey privateKey = asymmetricCryptography.getPrivate(PATH_KEYS+usuario.getUsername()+"/private_key");
				
				Mensagem mensagem = new Mensagem();
				mensagem.setConteudo(asymmetricCryptography.encryptText(conteudo, privateKey));
				mensagem.setRemetente(usuarioRecuperado);
				mensagem.setStatus(false);
				mensagem.setConversa(conversa);
				mensagemRepository.save(mensagem);
				
				conversa.getMensagens().add(mensagem);
				conversaRepository.save(conversa);
				
			} catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException | NoSuchPaddingException | IllegalBlockSizeException | InvalidKeyException | BadPaddingException e) {
				e.printStackTrace();
			}
			
			
		}
	}

	@Override
	public void excluirMensagem(Integer idMensagem, Integer idConversa) {
		Mensagem mensagem = mensagemRepository.findOne(idMensagem);
		Conversa conversa = conversaRepository.findOne(idConversa);
		
		if(mensagem != null && conversa != null) {
			mensagemRepository.delete(mensagem);
		}
		
	}

}
