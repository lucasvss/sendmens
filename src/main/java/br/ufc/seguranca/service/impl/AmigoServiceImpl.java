package br.ufc.seguranca.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.seguranca.exception.SendMensException;
import br.ufc.seguranca.model.Amigo;
import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.repository.AmigoRepository;
import br.ufc.seguranca.repository.ConversaRepository;
import br.ufc.seguranca.repository.UsuarioRepository;
import br.ufc.seguranca.service.AmigoService;

import static br.ufc.seguranca.util.Constants.ERROR_ADICIONAR_AMIGO;

@Service
public class AmigoServiceImpl implements AmigoService{

	@Autowired
	private AmigoRepository amigoRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private ConversaRepository conversaRepository;
	
	@Override
	public void adicionarAmigo(Amigo amigo, Usuario usuario) throws SendMensException {
		Usuario usuarioAmigo = usuarioRepository.findByUsername(amigo.getUsername());
		
		if(usuario != null && amigo != null) {
			amigo.setBloqueado(false);
			amigo.setUsuario(usuarioAmigo);
			amigoRepository.save(amigo);
			
			usuario.getAmigos().add(amigo);
			usuarioRepository.save(usuario);
		} else {
		
			throw new SendMensException(ERROR_ADICIONAR_AMIGO);
		}
	}
	
	@Override
	public void adicionarAmigo(Integer idAmigo, Usuario usuario) {
		Usuario usuarioAmigo = usuarioRepository.findOne(idAmigo);
		
		if(usuarioAmigo != null && usuario != null) {
			Amigo amigo = new Amigo();
			amigo.setApelido(usuarioAmigo.getNome());
			amigo.setUsername(usuarioAmigo.getUsername());
			amigo.setBloqueado(false);
			amigo.setUsuario(usuarioAmigo);
			amigoRepository.save(amigo);
			
			usuario.getAmigos().add(amigo);
			usuarioRepository.save(usuario);
		}
	}

	@Override
	public void alterarAmigo(Amigo amigo) {
		if(amigo != null) {
			amigoRepository.save(amigo);
		}
	}

	@Override
	public void bloquearAmigo(Integer idAmigo) {
		Amigo amigo = amigoRepository.findOne(idAmigo);
		
		if(amigo != null) {
			amigo.setBloqueado(true);
		}
	}

	@Override
	public void removerAmigo(Integer idAmigo) {
		Amigo amigo = amigoRepository.findOne(idAmigo);
		
		if(amigo != null) {
			amigoRepository.delete(amigo);
		}
	}

	@Override
	public Amigo buscarAmigo(Integer idAmigo) {
		return amigoRepository.findOne(idAmigo);
	}
	
	@Override
	public Amigo buscarAmigoConversa(Integer idConversa, Usuario usuario) {
		Conversa conversa = conversaRepository.findOne(idConversa);
		
		if(conversa != null && usuario != null) {
			if(conversa.getParticipanteIniciante().getUsername().equals(usuario.getUsername())) {
				for(Amigo amigo : usuario.getAmigos()) {
					if(amigo.getUsername().equals(conversa.getParticipanteChamado().getUsername())) {
						return amigo;
					}
				}
			} else {
				for(Amigo amigo : usuario.getAmigos()) {
					if(amigo.getUsername().equals(conversa.getParticipanteIniciante().getUsername())) {
						return amigo;
					}
				}
			}
		}
		
		return new Amigo();
	}
}