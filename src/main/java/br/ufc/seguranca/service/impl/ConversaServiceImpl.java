package br.ufc.seguranca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.seguranca.model.Amigo;
import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.repository.AmigoRepository;
import br.ufc.seguranca.repository.ConversaRepository;
import br.ufc.seguranca.repository.UsuarioRepository;
import br.ufc.seguranca.service.ConversaService;

@Service
public class ConversaServiceImpl implements ConversaService {

	@Autowired
	private ConversaRepository conversaRepository;
	
	@Autowired
	private AmigoRepository amigoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public void iniciarConversa(Usuario usuario, Integer idAmigo) {
		Amigo amigo = amigoRepository.findOne(idAmigo);
		Conversa conversa = conversaRepository.buscarConversa(usuario, amigo.getUsuario());
		
		if(conversa == null) {
			Conversa novaConversa = new Conversa();
			novaConversa.setParticipanteIniciante(usuario);
			novaConversa.setParticipanteChamado(amigo.getUsuario());
			
			conversaRepository.save(novaConversa);
			
			usuario.getConversas().add(novaConversa);
			amigo.getUsuario().getConversas().add(novaConversa);
			
			usuarioRepository.save(usuario);
			usuarioRepository.save(amigo.getUsuario());
		}
	}

	@Override
	public void excluirConversa(Integer idConversa) {
		Conversa conversa = conversaRepository.findOne(idConversa);
		
		if(conversa != null) {
			conversaRepository.delete(conversa);
		}
	}
	
	@Override
	public Conversa conversa(Integer idConversa) {
		return conversaRepository.findOne(idConversa);
	}
	
	@Override
	public Conversa conversa(Usuario usuario, Integer idAmigo) {
		Amigo amigo = amigoRepository.findOne(idAmigo);
		
		if(amigo == null && usuario == null) {
			return new Conversa();
		}
		
		return conversaRepository.buscarConversa(usuario, amigo.getUsuario());
			
	}
	
	@Override
	public List<Conversa> conversas(Usuario usuario) {
		return conversaRepository.buscarConversasUsuario(usuario);
	}
	

}
