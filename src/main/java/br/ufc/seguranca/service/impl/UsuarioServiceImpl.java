package br.ufc.seguranca.service.impl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.repository.UsuarioRepository;
import br.ufc.seguranca.service.UsuarioService;
import br.ufc.seguranca.util.GenerateKeys;

import static br.ufc.seguranca.util.Constants.PATH_KEYS;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private GenerateKeys generateKeys;
	
	@Override
	public void cadastrarUsuario(Usuario usuario) {
		if(usuario != null) {
			try {
				generateKeys = new GenerateKeys(1024);
				generateKeys.createKeys();
				generateKeys.writeToFile(PATH_KEYS+usuario.getUsername()+"/public_key", generateKeys.getPublicKey().getEncoded());
				generateKeys.writeToFile(PATH_KEYS+usuario.getUsername()+"/private_key", generateKeys.getPrivateKey().getEncoded());
				usuario.encodePassword();
				usuarioRepository.save(usuario);

			} catch (NoSuchAlgorithmException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void alterarUsuario(Usuario usuario) {
		if(usuario != null) {
			usuarioRepository.save(usuario);
		}
	}

	@Override
	public void excluirUsuario(Integer id) {
		Usuario usuario = usuarioRepository.findOne(id);	
		
		if(usuario != null){
			usuarioRepository.delete(usuario);
		}
	}

	@Override
	public Usuario recuperarUsuario(Integer id) {
		return usuarioRepository.findOne(id);
	}

}
