package br.ufc.seguranca.service;

import br.ufc.seguranca.model.Usuario;

public interface MensagemService {

	void enviarMensagem(String Conteudo, Usuario usuario, Integer idConversa);
	void excluirMensagem(Integer idMensagem, Integer idConversa);
	
}
