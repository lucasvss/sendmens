package br.ufc.seguranca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.service.ConversaService;
import br.ufc.seguranca.service.UsuarioService;

@ControllerAdvice(basePackages={"br.ufc.seguranca.controller"})
public class AttributesController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ConversaService conversaService;
	
	
	@ModelAttribute("usuarioLogado")
	public Usuario usuarioLogado(Authentication auth) {
		if(auth != null) {
			return usuarioService.recuperarUsuario(((Usuario) auth.getPrincipal()).getId());
		}
		
		return null;
	}
	
	@ModelAttribute("conversas")
	public List<Conversa> conversas(Authentication auth) {
		if(auth != null) {
			return conversaService.conversas(usuarioService.recuperarUsuario(((Usuario) auth.getPrincipal()).getId()));
		}
		
		return null;
	}
	
}
