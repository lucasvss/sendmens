package br.ufc.seguranca.controller;

import static br.ufc.seguranca.util.Constants.REDIRECT_PRINCIPAL;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.ufc.seguranca.exception.SendMensException;
import br.ufc.seguranca.model.Amigo;
import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.service.AmigoService;

@Controller
@RequestMapping("/usuario/amigo")
public class AmigoController {

	@Autowired
	private AmigoService amigoService;
	
	@PostMapping("/adicionar")
	public String adicionarAmigo(@Valid @ModelAttribute("amigo") Amigo amigo, Authentication auth) {
		Usuario usuario = (Usuario) auth.getPrincipal();
		try {
			amigoService.adicionarAmigo(amigo, usuario);
		} catch (SendMensException e) {
			e.printStackTrace();
		}
		return REDIRECT_PRINCIPAL;
	}
	
	@GetMapping("/adicionar/{id}")
	public String adicionarAmigo(@PathVariable("id") Integer idAmigo, Authentication auth) {
		Usuario usuario = (Usuario) auth.getPrincipal();
		amigoService.adicionarAmigo(idAmigo, usuario);
		return REDIRECT_PRINCIPAL;
	}
	
	@PostMapping("/alterar")
	public ModelAndView alterarAmigo(@ModelAttribute("amigo") Amigo amigo) {
		amigoService.alterarAmigo(amigo);
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
	
	@GetMapping("/bloquear/{id}")
	public ModelAndView bloquearAmigo(@PathVariable("id") Integer idAmigo) {
		amigoService.bloquearAmigo(idAmigo);
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
	
	@GetMapping("/remover/{id}")
	public ModelAndView removerAmigo(@PathVariable("id") Integer idAmigo) {
		amigoService.removerAmigo(idAmigo);
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
}
