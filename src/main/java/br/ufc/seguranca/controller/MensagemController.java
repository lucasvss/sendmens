package br.ufc.seguranca.controller;

import static br.ufc.seguranca.util.Constants.REDIRECT_PRINCIPAL;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.service.AmigoService;
import br.ufc.seguranca.service.ConversaService;
import br.ufc.seguranca.service.MensagemService;

@Controller
@RequestMapping("/usuario/mensagem")
public class MensagemController {

	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private ConversaService conversaService;
	
	@Autowired
	private AmigoService amigoService;
	
	@PostMapping("/enviar/{idConversa}")
	public ModelAndView enviarMensagem(String conteudo, @PathVariable("idConversa") Integer idConversa, RedirectAttributes redirectAttributes, Authentication auth) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, IOException, NoSuchPaddingException {
		Usuario usuario = (Usuario) auth.getPrincipal();
		mensagemService.enviarMensagem(conteudo, usuario, idConversa);
		redirectAttributes.addFlashAttribute("conversa", conversaService.conversa(idConversa));
		redirectAttributes.addFlashAttribute("amigo", amigoService.buscarAmigoConversa(idConversa, usuario));
		
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
	
	@GetMapping("/excluir/{idConversa}/{idMensgaem}")
	public ModelAndView excluirMensagem(@PathVariable("idMensagem") Integer idMensagem, @PathVariable("idConversa") Integer idConversa) {
		mensagemService.excluirMensagem(idMensagem, idConversa);
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
	
}
