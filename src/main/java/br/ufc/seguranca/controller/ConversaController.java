package br.ufc.seguranca.controller;

import static br.ufc.seguranca.util.Constants.REDIRECT_PRINCIPAL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.service.AmigoService;
import br.ufc.seguranca.service.ConversaService;

@Controller
@RequestMapping("/usuario/conversa")
public class ConversaController {
	
	@Autowired
	private ConversaService conversaService;
	
	@Autowired
	private AmigoService amigoService;
	
	@GetMapping("/{id}")
	public String mostrarConversa(@PathVariable("id") Integer idConversa, Authentication auth, RedirectAttributes redirectAttributes) {
		Usuario usuario = (Usuario) auth.getPrincipal();
		redirectAttributes.addFlashAttribute("conversa", conversaService.conversa(idConversa));
		redirectAttributes.addFlashAttribute("amigo", amigoService.buscarAmigoConversa(idConversa, usuario));
		
		return REDIRECT_PRINCIPAL;
	}
	
	@GetMapping("/amigo/{id}")
	public String mostrarConversaAmigo(@PathVariable("id") Integer idAmigo, Authentication auth, RedirectAttributes redirectAttributes) {
		Usuario usuario = (Usuario) auth.getPrincipal();
		conversaService.iniciarConversa(usuario, idAmigo);
		redirectAttributes.addFlashAttribute("conversa", conversaService.conversa(usuario, idAmigo));
		redirectAttributes.addFlashAttribute("amigo", amigoService.buscarAmigo(idAmigo));
		return REDIRECT_PRINCIPAL;
	}
	
	@GetMapping("/excluir/{id}")
	public String excluirConversa(@PathVariable("id") Integer idConversa) {
		conversaService.excluirConversa(idConversa);
		return REDIRECT_PRINCIPAL;
	}
}
