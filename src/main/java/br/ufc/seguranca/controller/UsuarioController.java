package br.ufc.seguranca.controller;

import static br.ufc.seguranca.util.Constants.MESSAGE_CONTEUDO_CADASTRO_USUARIO;
import static br.ufc.seguranca.util.Constants.MESSAGE_CONTEUDO_EDITAR_USUARIO;
import static br.ufc.seguranca.util.Constants.MESSAGE_CONTEUDO_RESETAR_SENHA;
import static br.ufc.seguranca.util.Constants.MESSAGE_STATUS_SUCCESS;
import static br.ufc.seguranca.util.Constants.MESSAGE_TITULO_CADASTRO_USUARIO;
import static br.ufc.seguranca.util.Constants.MESSAGE_TITULO_EDITAR_USUARIO;
import static br.ufc.seguranca.util.Constants.MESSAGE_TITULO_RESETAR_SENHA;
import static br.ufc.seguranca.util.Constants.REDIRECT_LOGIN;
import static br.ufc.seguranca.util.Constants.REDIRECT_LOGOUT;
import static br.ufc.seguranca.util.Constants.REDIRECT_PRINCIPAL;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.seguranca.model.Usuario;
import br.ufc.seguranca.service.UsuarioService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("cadastrar")
	public ModelAndView cadastrar(@Valid @ModelAttribute("usuario") Usuario usuario, RedirectAttributes redirectAttribute) {
		try {
			usuarioService.cadastrarUsuario(usuario);
			redirectAttribute.addFlashAttribute("status", MESSAGE_STATUS_SUCCESS.toString());
			redirectAttribute.addFlashAttribute("titulo", MESSAGE_TITULO_CADASTRO_USUARIO);
			redirectAttribute.addFlashAttribute("conteudo", MESSAGE_CONTEUDO_CADASTRO_USUARIO);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView(REDIRECT_LOGIN);
	}
	
	@GetMapping("reset")
	public String reset() {
		return "usuario/esqueceu-senha";
	}
	
	
	@PostMapping("/editar")
	public ModelAndView alterarUsuario(@ModelAttribute("usuario") Usuario usuario, RedirectAttributes redirectAttribute) {
		try {
			usuarioService.alterarUsuario(usuario);
			redirectAttribute.addFlashAttribute("status", MESSAGE_STATUS_SUCCESS);
			redirectAttribute.addFlashAttribute("titulo", MESSAGE_TITULO_EDITAR_USUARIO);
			redirectAttribute.addFlashAttribute("conteudo", MESSAGE_CONTEUDO_EDITAR_USUARIO);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
	
	@GetMapping("/removerConta")
	public ModelAndView excluirUsuario(Authentication auth, RedirectAttributes redirectAttribute) {
		Usuario usuario = (Usuario) auth.getPrincipal();
		
		try {
			usuarioService.excluirUsuario(usuario.getId());
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView(REDIRECT_LOGOUT);
	
	}
	
	@GetMapping("reset/{email}")
	public ModelAndView reset(@PathVariable("email") String email, RedirectAttributes redirectAttribute) {
		try {
			redirectAttribute.addFlashAttribute("status", MESSAGE_STATUS_SUCCESS);
			redirectAttribute.addFlashAttribute("titulo", MESSAGE_TITULO_RESETAR_SENHA);
			redirectAttribute.addFlashAttribute("conteudo", MESSAGE_CONTEUDO_RESETAR_SENHA);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView(REDIRECT_PRINCIPAL);
	}
}
