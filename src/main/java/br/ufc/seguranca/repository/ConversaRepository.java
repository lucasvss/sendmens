package br.ufc.seguranca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufc.seguranca.model.Conversa;
import br.ufc.seguranca.model.Usuario;


@Repository
public interface ConversaRepository extends CrudRepository<Conversa, Integer>{
	
	@Query("SELECT c FROM Conversa AS c WHERE c.participanteIniciante = :pI AND c.participanteChamado = :pC OR c.participanteIniciante = :pC AND c.participanteChamado = :pI")
	Conversa buscarConversa(@Param("pI") Usuario participanteIniciante, @Param("pC") Usuario participanteChamado);
	
	@Query("SELECT c FROM Conversa AS c WHERE c.participanteIniciante = :user OR c.participanteChamado = :user")
	List<Conversa> buscarConversasUsuario(@Param("user") Usuario usuario);
}
