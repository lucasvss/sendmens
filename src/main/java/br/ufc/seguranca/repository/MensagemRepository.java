package br.ufc.seguranca.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.seguranca.model.Mensagem;


@Repository
public interface MensagemRepository extends CrudRepository<Mensagem, Integer>{

}
