package br.ufc.seguranca.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.seguranca.model.Amigo;


@Repository
public interface AmigoRepository extends CrudRepository<Amigo, Integer>{
	
}
