package br.ufc.seguranca.exception;

public class SendMensException extends Exception{

	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public SendMensException(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
}
